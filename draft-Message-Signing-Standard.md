---
title: Arbitrary Message Signing Standard
status: Draft
author: @fredcy fredcy@gmail.com, JarrodWoodard jarrodwoodard@protonmail.com
type: TZIP
created: 2022-05-02
requires: None
replaces: None
replacing
superseded-by: None
current TZIP
date: 2022-05-02
version: 0
---


## Summary

The standard for arbitrary cryptographic message signing within the Tezos ecosystem.

## Abstract

This standard describes the best practices for use of arbitrary cryptographic message signing within the Tezos ecosystem. Especially with regards to use in decentralized applications (dapps). All dapps rely on message signing at one point or another. A 'message' can be any arbitray string of data OR a transaction that will have consequences on chain if 'signed' by one's private keys. 

This standard implements the `failing_noop` feature added to Tezos in the Florence upgrade. With the proper use of this feature, ANY arbitrary message can be signed with the guarantee that if injected as a transaction, it will by design, always fail. Put another way, it can do nothing on-chain.   

## Motivation

The goal of this standard tis o describe how to implement arbitrary message signing securely and to make the process as easy as possible for non-technical users to evaluate a message as safe to sign. 

## Specification

<<< Help me fredcy, you're my only hope! >>>

## Rationale

< The rationale fleshes out the specification by describing what motivated the
design and why particular design decisions were made. It should describe
alternate designs that were considered and related work. The rationale may also 
provide evidence of consensus within the community, and should discuss important 
objections or concerns raised during discussion. >

## Backwards Compatibility

< All TZIPs that introduce backwards incompatibilities or supersede other TZIPs
must include a section describing these incompatibilities, their severity, and
solutions. >

## Security Considerations

< This section of the document should explain any security relevant features
of the proposal, or any critical issues for implemenenters. >

## Test Cases

< Test cases for an implementation are recommended as are proofs of correctness via 
formal methods if applicable.>

## Implementations

< Any code already written. >

## Appendix

< A list of references relevant to the proposal. >

https://tezos.gitlab.io/user/various.html#failing-noop-operation

https://tezos.gitlab.io/shell/p2p_api.html?highlight=failing_noop#failing-noop-tag-17

https://gitlab.com/tezos/tezos/-/merge_requests/2361

## Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
